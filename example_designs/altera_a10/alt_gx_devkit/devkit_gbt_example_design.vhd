--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (EP-ESE-BE)                                                         
-- Engineer:              Julian Mendez (julian.mendez@cern.ch)
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           Altera GX development kit - GBT Bank example design                                        
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         Altera GX development kit (Altera Arria 10)                                                       
-- Tool version:          Quartus II 13.1                                                                
--                                                                                                   
-- Version:               3.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        15/04/2016   3.0       J. Mendez         First .vhd module definition           
--
-- Additional Comments:   Note!! This example design instantiates one GBt Banks:
--
--                               - GBT Bank 1: One GBT Link (Standard GBT TX and Standard GBT RX)     
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity devkit_gbt_example_design is   
   port (   
      
      --===============--
      -- General reset --
      --===============--  
      SYS_RESET_N                                             : in  std_logic; 
    
      --===============--
      -- Clocks scheme --
      --===============--
      REF_CLOCK                                            	  : in  std_logic;   -- Comment:  (LHC PLL 240MHz)
      SYS_CLK_100MHz														  : in  std_logic;
		SMA_CLK_OUT															  : out std_logic;
		
		SMATX_P																  : out std_logic;
		SMATX_N																  : out std_logic;
		
      --==============--
      -- Serial lanes --
      --==============--
      
      -- GBT Bank 1:
      --------------
		
      SFP_TX                                                  : out std_logic;
      SFP_RX                                           		  : in  std_logic;
		
		SFP_DISABLE															  : out std_logic
		
   );
end devkit_gbt_example_design;



--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of devkit_gbt_example_design is
   
   --================================ Signal Declarations ================================--	
   --=====================================================================================-- 
	
	--============--
	-- 40MHz PLL  --
	--============--
	signal SYS_CLK_40MHz							:	std_logic;
	signal frameclk_locked						: std_logic;	
	signal phaseShift_to_txpll 				: std_logic := '0';
	signal phaseShiftDone_from_txpll			: std_logic;
	
	--==============--
	-- XCVR Control --
	--==============--
	signal rx_pol									: std_logic := '1';
	signal tx_pol									: std_logic := '1';
	signal loopback								: std_logic := '1';
	
	--=====================--
	-- Measurements        --
	--=====================--
	signal tx_matchflag							: std_logic;
	signal rx_matchflag							: std_logic;
	
	signal rxframeclk_from_gbtExmplDsgn		: std_logic;
	signal txframeclk_from_gbtExmplDsgn		: std_logic;
	signal rxwordclk_from_gbtExmplDsgn		: std_logic;
	signal txwordclk_from_gbtExmplDsgn		: std_logic;
	
	signal latdelay								: std_logic_vector(31 downto 0);
	
	--============--
	-- GBT Status --
	--============--
	signal gbtData_from_gbtExmplDsgn			: std_logic_vector(83 downto 0);
	signal WBData_from_gbtExmplDsgn			: std_logic_vector(115 downto 0);
	signal gbtTxReady_from_gbtExamplDsgn	: std_logic;
	signal XCVRTXReady_from_gbtExamplDsgn 	: std_logic;
	signal XCVRRXReady_from_gbtExamplDsgn 	: std_logic;
	signal XCVRReady_from_gbtExamplDsgn 	: std_logic;
	signal gbtRxReady_from_gbtExamplDsgn	: std_logic;
	signal bitslip_from_gbtExamplDsgn		: std_logic_vector(5 downto 0);
	signal gbtRxLostFlag_from_gbtExmplDsgn	: std_logic;
	signal gbtDataErrSeen_from_gbtExmplDsgn: std_logic;
	signal WBDataErrSeen_from_gbtExmplDsgn	: std_logic;
	signal gbtErrorDetected						: std_logic;
	signal gbtModifiedBitFlag					: std_logic_vector(83 downto 0);
	signal headerFlag_from_gbtExmplDsgn		: std_logic;
	
	--==========--
	-- GBT Ctrl --
	--==========--
   signal txPllReset                                 : std_logic;
   signal shiftTxClock_from_vio                      : std_logic;
   signal txShiftCount_from_vio                      : std_logic_vector(7 downto 0);
	
	signal reset_from_issp						: std_logic;
	signal reset_from_jtag						: std_logic;
	signal resetTx_from_issp					: std_logic;
	signal resetRx_from_issp					: std_logic;
	
	signal reset_dataerrorseenflag			: std_logic;
	signal reset_gbtRxReadylostflag			: std_logic;
	
	signal gbtData_from_issp					: std_logic_vector(83 downto 0);
	signal WBData_from_issp						: std_logic_vector(115 downto 0);
	signal testPattern_from_issp				: std_logic_vector(1 downto 0);
	signal txIsDataSel_from_issp				: std_logic;
	signal rxIsDataSel_from_issp				: std_logic;
		
	signal fpllreconf_reset_reset : std_logic;                                        
	signal fpllreconf_address     : std_logic_vector(12 downto 0);                    
	signal fpllreconf_read        : std_logic;                                        
	signal fpllreconf_readdata    : std_logic_vector(31 downto 0); 
	signal fpllreconf_write       : std_logic;                                        
	signal fpllreconf_writedata   : std_logic_vector(31 downto 0);                    
	signal fpllreconf_waitrequest : std_logic;             
	
	signal phyreconf_reset_reset  : std_logic;                                        
	signal phyreconf_address      : std_logic_vector(12 downto 0);                    
	signal phyreconf_read         : std_logic;                                        
	signal phyreconf_readdata     : std_logic_vector(31 downto 0); 
	signal phyreconf_write        : std_logic;                                        
	signal phyreconf_writedata    : std_logic_vector(31 downto 0);                    
	signal phyreconf_waitrequest  : std_logic;  
		
	signal output_select_from_issp: std_logic;
	signal rxFrameclkRdy_from_gbtExmplDsgn : std_logic;
	
	signal rxFrameclk_alignPattern			: std_logic_vector(5 downto 0);
	signal generalReset						: std_logic;
	signal modifiedBitsCnt					: std_logic_vector(7 downto 0);
	signal countWordReceived				: std_logic_vector(31 downto 0);
	signal countBitsModified				: std_logic_vector(31 downto 0);
	signal countWordErrors					: std_logic_vector(31 downto 0);
	signal gbtModifiedBitFlagFiltered	: std_logic_vector(127 downto 0);
	
	signal txEncodingSel                : std_logic;
	signal rxEncodingSel                : std_logic;
	
	--============--
	-- Components --
	--============--
	component ax_issp is
		port (
			probe  : in  std_logic_vector(78 downto 0) := (others => 'X'); -- probe
			source : out std_logic_vector(26 downto 0)                     -- source
		);
	end component ax_issp;
	
	component ax_data_issp is
		port (
			probe  : in  std_logic_vector(201 downto 0) := (others => 'X'); -- probe
			source : out std_logic_vector(202 downto 0)                     -- source
		);
	end component ax_data_issp;
	
	component jtagToAvmm is
		port (
			bitslip_export            : in  std_logic_vector(5 downto 0)  := (others => '0'); --          bitslip.export
			clk_clk                   : in  std_logic                     := '0';             --              clk.clk
			gbt_fpll_0_reset_reset    : out std_logic;                                        -- gbt_fpll_0_reset.reset
			gbt_fpll_0_s0_address     : out std_logic_vector(12 downto 0);                    --    gbt_fpll_0_s0.address
			gbt_fpll_0_s0_read        : out std_logic;                                        --                 .read
			gbt_fpll_0_s0_readdata    : in  std_logic_vector(31 downto 0) := (others => '0'); --                 .readdata
			gbt_fpll_0_s0_write       : out std_logic;                                        --                 .write
			gbt_fpll_0_s0_writedata   : out std_logic_vector(31 downto 0);                    --                 .writedata
			gbt_fpll_0_s0_waitrequest : in  std_logic                     := '0';             --                 .waitrequest
			gbt_phy_0_reset_reset     : out std_logic;                                        --  gbt_phy_0_reset.reset
			gbt_phy_0_s0_address      : out std_logic_vector(12 downto 0);                    --     gbt_phy_0_s0.address
			gbt_phy_0_s0_read         : out std_logic;                                        --                 .read
			gbt_phy_0_s0_readdata     : in  std_logic_vector(31 downto 0) := (others => '0'); --                 .readdata
			gbt_phy_0_s0_write        : out std_logic;                                        --                 .write
			gbt_phy_0_s0_writedata    : out std_logic_vector(31 downto 0);                    --                 .writedata
			gbt_phy_0_s0_waitrequest  : in  std_logic                     := '0';             --                 .waitrequest
			gbtfpga_reset_export      : out std_logic;                                        --    gbtfpga_reset.export
			gbtrx_ready_export        : in  std_logic                     := '0';             --      gbtrx_ready.export
			output_select_export      : out std_logic; 
			clkdes_export             : in  std_logic_vector(5 downto 0)  := (others => '0');
			rxcount_export        	  : in  std_logic_vector(31 downto 0) := (others => '0');
			rxmodifiedcount_export    : in  std_logic_vector(31 downto 0) := (others => '0');
			rxworderr_export        	  : in  std_logic_vector(31 downto 0) := (others => '0');
			reset_reset_n             : in  std_logic                     := '0'              --            reset.reset_n
		);
	end component jtagToAvmm;
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================--


       --#############################################################################--
     --#################################################################################--
   --#############################                           #############################--
   --#############################  GBT Bank example design  #############################--
   --#############################                           #############################--
     --#################################################################################--
       --#############################################################################--
   
	--===================--
	-- SFP Configuration --
	--===================--
	SFP_DISABLE <= '0';

	--==================--
	-- TX Frameclk PLL  --
	--==================--
	
	-- In order to emulate a real environement, the 40MHz is created from the 
	-- Reference clock to control the phase relation between both clocks.
	--
	-- In a real system, the 40MHz and 240MHz come from an external PLL with
	-- a constant phase relationship.
	txFrameclkGen_inst: entity work.alt_a10_tx_phaligner
		  Port map( 
				-- Reset
				RESET_IN              => not(SYS_RESET_N), --txPllReset,
				
				-- Clocks
				CLK_IN                => REF_CLOCK,
				CLK_OUT               => SYS_CLK_40MHz,
				
				-- Control
				SHIFT_IN              => shiftTxClock_from_vio,
				SHIFT_COUNT_IN        => txShiftCount_from_vio,
				
				-- Status
				LOCKED_OUT            => frameclk_locked
		  );

	generalReset <= not(SYS_RESET_N) or reset_from_issp or reset_from_jtag;
	
   --=====================================--
   -- Arria 10 - GBT Bank example design  --
   --=====================================--	
	gbtExmplDsgn_inst: entity work.alt_ax_gbt_example_design
		generic map(
          NUM_LINKS                                              => NUM_LINK_Conf,                 -- Up to 4
          TX_OPTIMIZATION                                        => TX_OPTIMIZATION_Conf,          -- LATENCY_OPTIMIZED or STANDARD
          RX_OPTIMIZATION                                        => RX_OPTIMIZATION_Conf,          -- LATENCY_OPTIMIZED or STANDARD
          TX_ENCODING                                            => TX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
          RX_ENCODING                                            => RX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
          
          DATA_GENERATOR_ENABLE                                  => DATA_GENERATOR_ENABLE_Conf,
          DATA_CHECKER_ENABLE                                    => DATA_CHECKER_ENABLE_Conf,
          MATCH_FLAG_ENABLE                                      => MATCH_FLAG_ENABLE_Conf,
          CLOCKING_SCHEME                                        => CLOCKING_SCHEME_Conf
		)
      port map (

		--==============--
		-- Clocks       --
		--==============--
		FRAMECLK_40MHZ												=> SYS_CLK_40MHz,
		XCVRCLK														=> REF_CLOCK,
		
		RX_FRAMECLK_O(1)											=> rxframeclk_from_gbtExmplDsgn,
		TX_FRAMECLK_O(1)											=> txframeclk_from_gbtExmplDsgn,
		RX_WORDCLK_O(1)											=> rxwordclk_from_gbtExmplDsgn,
		TX_WORDCLK_O(1)											=> txwordclk_from_gbtExmplDsgn,
		
		--==============--
		-- Reset        --
		--==============--
		GBTBANK_GENERAL_RESET_I									=> generalReset,
		GBTBANK_MANUAL_RESET_TX_I								=> resetTx_from_issp,
		GBTBANK_MANUAL_RESET_RX_I								=> resetRx_from_issp,
		
		--==============--
		-- Serial lanes --
		--==============--
		GBTBANK_MGT_RX(1)											=> SFP_RX,
		GBTBANK_MGT_TX(1)											=> SFP_TX,
		
		--==============--
		-- Data			 --
		--==============--		
		GBTBANK_GBT_DATA_I(1)									=> gbtData_from_issp,
		GBTBANK_WB_DATA_I(1)										=> WBData_from_issp,
		
		GBTBANK_GBT_DATA_O(1)									=> gbtData_from_gbtExmplDsgn,
		GBTBANK_WB_DATA_O(1)										=> WBData_from_gbtExmplDsgn,
		
		--==============--
		-- Reconf.		 --
		--==============--
		GBTBANK_RECONF_AVMM_RST									=> phyreconf_reset_reset,
		GBTBANK_RECONF_AVMM_CLK									=> SYS_CLK_40MHz,
		GBTBANK_RECONF_AVMM_ADDR								=> phyreconf_address,
		GBTBANK_RECONF_AVMM_READ								=> phyreconf_read,
		GBTBANK_RECONF_AVMM_WRITE								=> phyreconf_write,
		GBTBANK_RECONF_AVMM_WRITEDATA							=> phyreconf_writedata,
		GBTBANK_RECONF_AVMM_READDATA							=> phyreconf_readdata,
		GBTBANK_RECONF_AVMM_WAITREQUEST						=> phyreconf_waitrequest,
			
		GBTBANK_TXPLL_RECONF_AVMM_RST							=> fpllreconf_reset_reset,
		GBTBANK_TXPLL_RECONF_AVMM_CLK							=> SYS_CLK_40MHz,
		GBTBANK_TXPLL_RECONF_AVMM_ADDR						=> fpllreconf_address(9 downto 0),
		GBTBANK_TXPLL_RECONF_AVMM_READ						=> fpllreconf_read,
		GBTBANK_TXPLL_RECONF_AVMM_WRITE						=> fpllreconf_write,
		GBTBANK_TXPLL_RECONF_AVMM_WRITEDATA					=> fpllreconf_writedata,
		GBTBANK_TXPLL_RECONF_AVMM_READDATA					=> fpllreconf_readdata,
		GBTBANK_TXPLL_RECONF_AVMM_WAITREQUEST				=> fpllreconf_waitrequest,
		
		--==============--
		-- TX ctrl	    --
		--==============--
      TX_ENCODING_SEL_i(1)                            => txEncodingSel,
		GBTBANK_TX_ISDATA_SEL_I(1)								=> txIsDataSel_from_issp,
		GBTBANK_TEST_PATTERN_SEL_I								=> testPattern_from_issp,
		
		--==============--
		-- RX ctrl      --
		--==============--
      RX_ENCODING_SEL_i(1)                            => rxEncodingSel,
		GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(1)     		=> reset_gbtRxReadylostflag,
		GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I(1)      		=> reset_dataerrorseenflag,
		GBTBANK_RXFRAMECLK_ALIGNPATTER_I					   => rxFrameclk_alignPattern,
		GBTBANK_RXBITSLIT_RSTONEVEN_I(1)						=> '1',
		
		--==============--
		-- TX Status    --
		--==============--
		GBTBANK_GBTTX_READY_O(1)								=> gbtTxReady_from_gbtExamplDsgn,
		GBTBANK_LINK_TX_READY_O(1)								=> XCVRTXReady_from_gbtExamplDsgn,
		GBTBANK_LINK_READY_O(1)									=> XCVRReady_from_gbtExamplDsgn,
		GBTBANK_TX_MATCHFLAG_O									=> tx_matchflag,
		
		--==============--
		-- RX Status    --
		--==============--
		GBTBANK_GBTRX_READY_O(1)								=> gbtRxReady_from_gbtExamplDsgn,
		GBTBANK_LINK_RX_READY_O(1)								=> XCVRRXReady_from_gbtExamplDsgn,
		--GBTBANK_LINK1_BITSLIP_O           					=> ,
		GBTBANK_GBTRXREADY_LOST_FLAG_O(1)               => gbtRxLostFlag_from_gbtExmplDsgn,
		GBTBANK_RXDATA_ERRORSEEN_FLAG_O(1)              => gbtDataErrSeen_from_gbtExmplDsgn,
		GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O(1) => WBDataErrSeen_from_gbtExmplDsgn,
		GBTBANK_RX_MATCHFLAG_O(1)								=> rx_matchflag,
		GBTBANK_RX_ISDATA_SEL_O(1)								=> rxIsDataSel_from_issp,
		
		GBTBANK_RX_ERRORDETECTED_O(1)							=> gbtErrorDetected,
		GBTBANK_RX_BITMODIFIED_FLAG_O(1)						=> gbtModifiedBitFlag,
		GBTBANK_RX_HEADERFLAG_O(1)								=> headerFlag_from_gbtExmplDsgn,
		GBTBANK_RX_FRAMECLK_READY_O(1)						=> rxFrameclkRdy_from_gbtExmplDsgn,
		GBTBANK_RXBITSLIP_RST_CNT_O(1)						=> open,
		
		--==============--
		-- XCVR ctrl    --
		--==============--
		GBTBANK_LOOPBACK_I(1)					=> loopback,
		
		GBTBANK_TX_POL(1)							=> tx_pol,
		GBTBANK_RX_POL(1)							=> rx_pol
   );
	
	bitslip_from_gbtExamplDsgn <= (others => '0');
	
	--=====================================--
	-- BER                                 --
	--=====================================--
	countWordReceivedProc: PROCESS(generalReset, rxframeclk_from_gbtExmplDsgn)
	begin
	
		if generalReset = '1' then
			countWordReceived <= (others => '0');
			countBitsModified <= (others => '0');
			countWordErrors	<= (others => '0');
			
		elsif rising_edge(rxframeclk_from_gbtExmplDsgn) then
			if gbtRxReady_from_gbtExamplDsgn = '1' then
				
				if gbtErrorDetected = '1' then
					countWordErrors	<= std_logic_vector(unsigned(countWordErrors) + 1 );				
				end if;
				
				countWordReceived <= std_logic_vector(unsigned(countWordReceived) + 1 );
			end if;
			
			countBitsModified <= std_logic_vector(unsigned(modifiedBitsCnt) + unsigned(countBitsModified) );
		end if;
	end process;
	
	gbtModifiedBitFlagFiltered(127 downto 84) <= (others => '0');
	gbtModifiedBitFlagFiltered(83 downto 0) <= gbtModifiedBitFlag when gbtRxReady_from_gbtExamplDsgn = '1' else
															 (others => '0');
	
	countOnesCorrected: entity work.CountOnes
		Generic map (SIZE           => 128,
					MAXOUTWIDTH    	=> 8)
		Port map( 
			Clock    => rxframeclk_from_gbtExmplDsgn,
         I        => gbtModifiedBitFlagFiltered,
         O        => modifiedBitsCnt);
			  
	--============--
	-- Debug issp --
	--============--	
	gbtBank1_inSysSrcAndPrb: ax_issp
      port map (
         PROBE(0)                                             => gbtTxReady_from_gbtExamplDsgn,
			PROBE(1)															  => gbtRxReady_from_gbtExamplDsgn,			
			PROBE(2)															  => XCVRTXReady_from_gbtExamplDsgn,
			PROBE(3)															  => XCVRRXReady_from_gbtExamplDsgn,
			PROBE(4)															  => XCVRReady_from_gbtExamplDsgn,			
			PROBE(10 downto 5)											  => bitslip_from_gbtExamplDsgn,			
			PROBE(11)														  => gbtRxLostFlag_from_gbtExmplDsgn,
			PROBE(12)														  => gbtDataErrSeen_from_gbtExmplDsgn,			
			PROBE(13)														  => WBDataErrSeen_from_gbtExmplDsgn,
			PROBE(14)														  => frameclk_locked,
			PROBE(46 downto 15)                                  => countBitsModified,
			PROBE(78 downto 47)                                  => countWordReceived,
			
         SOURCE(0)                                            => loopback,
         SOURCE(1)                                            => rx_pol,
         SOURCE(2)                                            => tx_pol,
         SOURCE(3)                                            => reset_dataerrorseenflag,
         SOURCE(4)                                            => reset_gbtRxReadylostflag,
         SOURCE(5)                                            => reset_from_issp,
         SOURCE(6)                                            => resetTx_from_issp,
         SOURCE(7)                                            => resetRx_from_issp,
			SOURCE(13 downto 8)											  => rxFrameclk_alignPattern,
			SOURCE(14)											  			  => shiftTxClock_from_vio,
			source(22 downto 15)											  => txShiftCount_from_vio,
			source(23)														  => txPllReset,
			source(24)														  => output_select_from_issp,
			source(25)                                           => txEncodingSel,
			source(26)                                           => rxEncodingSel
      );
		
	gbtBank1_Data_inSysSrcAndPrb: ax_data_issp
      port map (
         SOURCE(83 downto 0)                                  => gbtData_from_issp,
			SOURCE(199 downto 84)										  => WBData_from_issp,
			SOURCE(201 downto 200)										  => testPattern_from_issp,
			SOURCE(202)														  => txIsDataSel_from_issp,
			
         PROBE(83 downto 0)                                   => gbtData_from_gbtExmplDsgn,
			PROBE(199 downto 84)										  	  => WBData_from_gbtExmplDsgn,
			PROBE(200)														  => rxIsDataSel_from_issp,
			PROBE(201)														  => rxFrameclkRdy_from_gbtExmplDsgn
      );
		
	--======================--
	-- Latency measurements --
	--======================--
	
		-- The script latency_meas.tcl, located into the example_designs/altera_a10/tcl folder can be
		-- used to carry out automatic measurement of the latency.
		-- It performs multiple resets of the gbt IP and measure the latency between tx_matchflag and
		-- rx_matchflag. To use it, the test pattern shall be "01".
		
		latencymeas_inst: entity work.pattern_matchflag_delaymeas
			port map(   
				RESET_I               => reset_from_issp,
				CLK_I                 => REF_CLOCK,
				TX_MATCHFLAG_I        => tx_matchflag,
				RX_MATCHFLAG_I        => rx_matchflag,
				DELAY_O				    => latdelay
			);
	
	--========================--
	-- Latency optimized test --
	--========================--
	jtagToAvmm_inst: jtagToAvmm
		port map(
			clk_clk            	=> SYS_CLK_40MHz,
			reset_reset_n      	=> frameclk_locked,
			gbtfpga_reset_export => reset_from_jtag,
			gbtrx_ready_export   => gbtRxReady_from_gbtExamplDsgn,
			bitslip_export			=> bitslip_from_gbtExamplDsgn,
			output_select_export => open, --Not used anymore, configured using ISSP
			clkdes_export			=> "00000" & gbtDataErrSeen_from_gbtExmplDsgn,
			rxcount_export        	  => countWordReceived,
			rxmodifiedcount_export    => countBitsModified,
			rxworderr_export			  => countWordErrors,
			
			gbt_fpll_0_reset_reset    		=> fpllreconf_reset_reset,
			gbt_fpll_0_s0_address     		=> fpllreconf_address,
			gbt_fpll_0_s0_read        		=> fpllreconf_read,
			gbt_fpll_0_s0_readdata    		=> fpllreconf_readdata,
			gbt_fpll_0_s0_write       		=> fpllreconf_write,
			gbt_fpll_0_s0_writedata   		=> fpllreconf_writedata,
			gbt_fpll_0_s0_waitrequest 		=> fpllreconf_waitrequest,
			
			gbt_phy_0_reset_reset     		=> phyreconf_reset_reset,
			gbt_phy_0_s0_address      		=> phyreconf_address,
			gbt_phy_0_s0_read         		=> phyreconf_read,
			gbt_phy_0_s0_readdata     		=> phyreconf_readdata,
			gbt_phy_0_s0_write        		=> phyreconf_write,
			gbt_phy_0_s0_writedata    		=> phyreconf_writedata,
			gbt_phy_0_s0_waitrequest  		=> phyreconf_waitrequest
		);

	SMA_CLK_OUT <= txframeclk_from_gbtExmplDsgn when output_select_from_issp = '1' else
						tx_matchflag;
						
	SMATX_P <= txwordclk_from_gbtExmplDsgn when output_select_from_issp = '1' else
				  rx_matchflag;
	SMATX_N <= txwordclk_from_gbtExmplDsgn when output_select_from_issp = '1' else
				  rx_matchflag;
	
   --=====================================================================================--   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--